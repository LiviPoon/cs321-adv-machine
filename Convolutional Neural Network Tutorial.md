## Convolution Neural Network Tutorial

### Tutorial Code

```
from keras.models import Sequential

from keras.layers import Conv2D

import numpy

from matplotlib.pyplot import imshow

# tell matplotlib to display images within this notebook

%matplotlib inline

  

# Some fancy google colab stuff just for this Tutorial (the question tables)

from google.colab import widgets

kernel_size = 3 #adjust (means in this case a 3x3)

image_size = 9 #adjust (means in this case a 9x9)

model0 = Sequential()

model0.add(Conv2D(filters=1,kernel_size=kernel_size, strides=3, input_shape=(image_size, image_size, 1)))

weights = model0.get_weights()

weights

```

1. What do Kernal Size and Step Refer to?
	1.  Kernal Size is the size of the filter.
	2.  They meant to say stride, which is the amount of times the kernal has to go over the image.
	
2. Why are there nine numbers in the weight? 
	1. It has to do with the kernal size. For example of ```kernal_size = 3``` then then the output of weight will have 9 numbers.
	
3. What happens to the number of numbers in the weight when you change the kernal size to 9? Why? 
	1. As expected the amount of numbers in weight change from 3x3 to 9x9.


5. What happens to the number of numbers in the weight when you change the stride to 3? Why?
	1. The numbers in the output of weight doesn't change because the weight values provided back are just for the filter. Because the model hasn't used the filters yet, the number of strides won't change anything.

### Tutorial #2 Code

```
layer_num = 0

filter_num = 0

y = 0

for x in range(kernel_size):

	weights[layer_num][y][x][0][filter_num] = 1

for y in range(1,kernel_size):

	for x in range(kernel_size):

		weights[layer_num][y][x][0][filter_num] = -1

weights

model0.set_weights(weights)

image0 = numpy.array([

	[128, 0, 128, 255, 128, 0, 128, 255, 128],

	[128, 0, 128, 255, 128, 0, 128, 255, 128],

	[128, 0, 128, 255, 128, 0, 128, 255, 128],

	[128, 0, 128, 255, 128, 0, 128, 255, 128],

	[128, 0, 128, 255, 128, 0, 128, 255, 128],

	[128, 0, 128, 255, 128, 0, 128, 255, 128],

	[128, 0, 128, 255, 128, 0, 128, 255, 128],

	[128, 0, 128, 255, 128, 0, 128, 255, 128],

	[128, 0, 128, 255, 128, 0, 128, 255, 128],

], dtype=numpy.uint8)

imshow(image0, cmap='gray')

image1 = numpy.array([

	[128, 128, 128, 128, 128, 128, 128, 128, 128],

	[0, 0, 0, 0, 0, 0, 0, 0, 0],

	[128, 128, 128, 128, 128, 128, 128, 128, 128],

	[255, 255, 255, 255, 255, 255, 255, 255, 255],

	[128, 128, 128, 128, 128, 128, 128, 128, 128],

	[0, 0, 0, 0, 0, 0, 0, 0, 0],

	[128, 128, 128, 128, 128, 128, 128, 128, 128],

	[255, 255, 255, 255, 255, 255, 255, 255, 255],

	[128, 128, 128, 128, 128, 128, 128, 128, 128],

], dtype=numpy.uint8)

imshow(image1, cmap='gray')

images = []

for image in [image0, image1]: # You may find it easier to take one of 		these out, to look at them one at a time

	images.append(numpy.resize(image, (image_size, image_size, 1))) 

images[0]

model0.predict(numpy.array(images))



```

Weight represents a scale to determine if the pixel we are looking at is the pixel we want. So if we are looking at a black pixel and we want to see a white pixel, then the weight is -1. If we are looking at a white pixel and we want to see a white pixel then the weight would be 1

1. There are a lot of numbers in the output above: 2 arrays of 7 arrays of 7 arrays of a single element. Why are they in groups of seven? 
	1. Because the filter is 3 across, and we are running the filter with a stride of 1 --> For each row we running the filter 7 times. Thus we get 7 numbers per row. 
	2. The values of the predict function is higher than the model is saying that that particular values are closer to what the filter is looking for (or what it is). If the values of the predict function is lower than the model is saying that that particular area is further than what the filter is looking for.


2. When we created the model, we asked it to have one filter. In which image do we get the highest absolute values in the filter outputs? How does this relate to the pattern of weights that was set?
	1. The second image, ```image1``` would have higher absolute values, because the filter is looking for a horizontal line (we don't, however, know if it's a white horizontal or a black horizontal line).


4. Look at your Filter again and at the predictions. What do you think the 1 and -1 represent? 
	1. 1 represents white pixels, and -1 represents black pixels.


6. What would this filter be useful for finding? 
	1. Horizontal white lines.


8. Go back to where you set your images, filter, strides, or some other hyper parameters and try changing the values. How does this change the outcomes and outputs?
	1. images will change the output of predict because the kernal/filter will be different. Changing the value 


#### Day 2 (techincally 3)

Recognizing numbers 
	1st layer: identifying features of numbers
	2nd layer: identify how/where features combine
	
What are the siginficance of the number of layers in an CNN?
	The more layers you have the more detailed your filter could be. It could also be usefull in bigger images.

How do layers work in CNNs?
	Say we have two filters, the way layers work in CNN's is it first would run the filters on the input image and then determine an output score, then repeat for the second filter. The next layer would be trying to take the filters from the previous layer and find a way to meld them into the same layer so as to identify a better filter that produces an output that is better than the two outputs produced from the beginning filters you started out with.