## Reinforcement Learning.

### What is reinforcement learning?

This type of machine learning creates models that assign a point system to the actions or output of the particular model. When the model "does the right thing" it is given points (thus it knows it is doing the right thing), when the model "does the wrong thing" then points are taken away (thus it knows it is doing the wrong thing). ""*The points are determined by the person creating the Model, not the training itself.  The training determines the "correct moves" based on the state.*"

These type of machine learning that is commonly used in video games.

The "weight" in reinforcement learning is correctiveness in a given state, what is the right action in a given state. 

### What are some negatives of using Reinforcement Learning?

It would take a lot of time to train. It also needs a human aspect to watch and teach the model what is "right".

### Mapping actions to points

Each game/round that the AI plays is called an *episode*.

Q is a function given **(state, action) => Reward**

A state can have muiltiple actions, each with a seperate reward attached to it.

### Deep Q vs. Q Table

- Deep Q
	- What is Deep Q?
		- 
	- Possible Process
		- Creates an empty array.
		- For every state, a random state is checked against the array. 
		- Agent identifies action that has the biggest reward.
		- Agent adds/updates the state to/in the array with the action and corresponding reward.

- Q Table
	- What is Q Table
	- Possible Process
		- Creates an array with every possible state, and within each possible state *num* amount of actions and the reward recieved for doing that action.
		- For every state, the state is checked against the array.
		- Agent identifies action that has the biggest reward.
		- Agent updates the state with the action and corresponding reward.


